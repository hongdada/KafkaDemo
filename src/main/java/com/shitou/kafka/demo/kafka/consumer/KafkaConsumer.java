package com.shitou.kafka.demo.kafka.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author qhong
 * @date 2019/9/9 16:53
 **/
@Component
@Slf4j
public class KafkaConsumer {
//	@KafkaListener(topics = {"testTopic"})
//	public void listen(ConsumerRecord<?, ?> record) {
//
//		Optional<?> kafkaMessage = Optional.ofNullable(record.value());
//
//		if (kafkaMessage.isPresent()) {
//
//			Object message = kafkaMessage.get();
//
//			log.info("----------------- record =" + record);
//			log.info("------------------ message =" + message);
//		}
//
//	}
}
