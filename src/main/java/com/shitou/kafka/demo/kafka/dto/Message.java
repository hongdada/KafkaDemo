package com.shitou.kafka.demo.kafka.dto;

import java.util.Date;
import lombok.Data;

/**
 * @author qhong
 * @date 2019/9/9 16:50
 **/
@Data
public class Message {
	private Long id;

	private String msg;

	private Date sendTime;

}