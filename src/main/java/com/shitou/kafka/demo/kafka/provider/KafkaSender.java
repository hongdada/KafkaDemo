package com.shitou.kafka.demo.kafka.provider;

import com.alibaba.fastjson.JSON;
import com.shitou.kafka.demo.kafka.dto.Message;
import java.util.Date;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @author qhong
 * @date 2019/9/9 16:50
 **/
@Component
@Slf4j
public class KafkaSender {

	@Autowired
	private KafkaTemplate kafkaTemplate;


	public void send() {
		Message message = new Message();
		message.setId(System.currentTimeMillis());
		message.setMsg(UUID.randomUUID().toString());
		message.setSendTime(new Date());
		log.info("+++++++++++++++++++++  message = {}", JSON.toJSONString(message));
		kafkaTemplate.send("testTopic", JSON.toJSONString(message));
	}
}