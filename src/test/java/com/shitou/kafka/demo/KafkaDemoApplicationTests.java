package com.shitou.kafka.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class KafkaDemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private KafkaTemplate kafkaTemplate;

	@Test
	public void producer() {
		kafkaTemplate.send("testTopic", "msgValue");
	}
}
